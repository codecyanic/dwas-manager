# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import socket
from dwas import Manifest, run

from .app import app
from multiprocessing import Process
from waitress import serve

HOST = '127.0.0.1'


def choose_port():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, 0))
    port = sock.getsockname()[1]
    sock.close()
    return port


def wsgi(port):
    serve(app, host=HOST, port=port)


def main():
    port = choose_port()

    p = Process(target=wsgi, args=(port,))
    p.start()

    url = 'http://%s:%d/' % (HOST, port)
    manifest = {
        'name': 'DWAS Manager',
        'url': url,
        'window': {'width': 1024, 'height': 624},
        'allow': [url + '.*', 'data:.*'],
    }
    manifest = Manifest(json.dumps(manifest))

    run(manifest)

    p.terminate()


if __name__ == '__main__':
    main()
