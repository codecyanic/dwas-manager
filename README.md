# ![Logo](dwas-manager.svg "Yes, this is a sand-box-manager") DWAS Manager

A GUI for running and managing DWAS applications.

## Requirements

You will need to build [DWAS](https://gitlab.com/codecyanic/dwas) from source and install it.

In order to run the software, you also need Flask, PyYAML, Waitress, and Setuptools installed.

On Debian/Ubuntu run:
```
apt install python3-flask python3-yaml python3-waitress python3-setuptools
```

## Running

For testing, you can run the program from the source directory with:
```
python3 -m dwas_manager
```

To install for the current user, run:
```
./setup.py install --user
```

After installing you should also be able to run the program using the `dwas-manager` command.
If not, you may need to log in to your account again or add the directory of the executable to path.

## Usage notes

For more information on how to edit app manifests, take a look at the [DWAS](https://gitlab.com/codecyanic/dwas#manifest-files) project.
