#!/usr/bin/env python3

# Copyright 2020 Cyanic
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys

from setuptools import setup


def data_files():
    share_dir = os.path.join(sys.prefix, 'share')
    if not share_dir.startswith('/usr/') or not os.access(share_dir, os.W_OK):
        share_dir = os.path.expanduser('~/.local/share')

    icons_dir = os.path.join(share_dir, 'icons', 'hicolor', 'scalable', 'apps')
    icons = ['dwas-manager.svg', 'dwas_manager/static/dwas-app.svg']
    return [
        (icons_dir, icons),
        (os.path.join(share_dir, 'applications'), ['dwas-manager.desktop']),
    ]


setup(
    name='DWAS Manager',
    version='0.5.2',
    packages=['dwas_manager'],
    entry_points={
        'console_scripts': [
            'dwas-manager = dwas_manager.__main__:main',
        ],
    },
    data_files=data_files(),
    include_package_data=True,
    zip_safe=False,
)
